package com.mangal.test.data

import com.google.gson.annotations.SerializedName

data class GetTestDataResponse(
    @SerializedName("statusCode") val statusCode: Int,
    @SerializedName("APICODERESULT") val APICODERESULT: String,
    @SerializedName("result")
    val data: TestData
) {
    data class TestData(
        @SerializedName("description_title") val desTitle: String,
        @SerializedName("decription_image") val decriptionImage: String,
        @SerializedName("description_body") val descriptionBody: String,
        @SerializedName("banner") val banner: List<String>,
        @SerializedName("latitudes") val latitudes: String,
        @SerializedName("longitude") val longitude: String,
        @SerializedName("cupons") val cupons: List<Cupons>


    ) {
        data class Cupons(
            @SerializedName("title") val title: String,
            @SerializedName("description") val description: String,
            @SerializedName("price") val price: String
        )
    }
}