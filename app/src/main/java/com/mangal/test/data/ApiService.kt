package com.mangal.test.data

import androidx.lifecycle.LiveData
import com.mangal.test.BuildConfig
import com.mangal.test.util.ApiResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    companion object {
        var baseUrl: String = BuildConfig.BASE_URL
    }


    @FormUrlEncoded
    @POST(value = "login")
    fun postLoginData(@Field("email") email:String,@Field("password") password:String): Call<ResponseBody>

    @GET("testing_data")
    fun getTestData(): LiveData<ApiResponse<GetTestDataResponse>>


}
