package com.mangal.test.data

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("id") val id: String, @SerializedName("name") val name: String,
    @SerializedName("email") val email: String, @SerializedName("utype") val utype: String
)



