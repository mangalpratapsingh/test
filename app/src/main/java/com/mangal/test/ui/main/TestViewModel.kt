package com.mangal.test.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mangal.test.data.ApiService
import com.mangal.test.data.GetTestDataResponse
import com.mangal.test.util.ApiResponse
import javax.inject.Inject

class TestViewModel @Inject constructor(private val apiService: ApiService):ViewModel() {
    fun getTestData(): LiveData<ApiResponse<GetTestDataResponse>> {
        return apiService.getTestData()
    }
}