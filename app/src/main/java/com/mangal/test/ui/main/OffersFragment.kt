package com.mangal.test.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mangal.test.R
import com.mangal.test.data.GetTestDataResponse
import com.mangal.test.data.User
import com.mangal.test.databinding.FragmentOffersBinding
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment

class OffersFragment() : DaggerFragment(),
    MenuListAdapter.OnNumberListener {


    private var data: ArrayList<GetTestDataResponse.TestData.Cupons> = arrayListOf()
    private lateinit var binding: FragmentOffersBinding
    //private lateinit var sectionsPagerAdapter: SectionsPagerAdapter

    private lateinit var offerListAdapter: OfferListAdapter
  //  private lateinit var menuListAdapter: MenuListAdapter
  //  private  var dataPointList:ArrayList<User>? = arrayListOf()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offers, container, false)
        binding.setLifecycleOwner(this)


     //   sectionsPagerAdapter = SectionsPagerAdapter(context = activity!!, data = data.banner)
//        binding.photosViewpager.adapter = sectionsPagerAdapter
//        binding.tabIndicater.setupWithViewPager(binding.photosViewpager)

        data.add(GetTestDataResponse.TestData.Cupons("Ravi Singh","ravisingh@gmail.com","500"))
        data.add(GetTestDataResponse.TestData.Cupons("Pavam Yadav","pavan@gmail.com","500"))

        data.add(GetTestDataResponse.TestData.Cupons("Mahesh Bhandary","Mahesh@gmail.com","500"))
        data.add(GetTestDataResponse.TestData.Cupons("Manish Singh","manish@gmail.com","500"))

        data.add(GetTestDataResponse.TestData.Cupons("mukesh chabra","mukesh_chabra@gmail.com","500"))

        data.add(GetTestDataResponse.TestData.Cupons("Mayank yadav","mayank@gmail.com","500"))

        data.add(GetTestDataResponse.TestData.Cupons("Priti kumari","priti@gmail.com","500"))



//


     /*   menuListAdapter = MenuListAdapter(activity!!, dataPointList, object : MenuListAdapter.OnNumberListener {

            @SuppressLint("LogNotTimber")
            override fun onNumberClick(position: Int?) {
                Log.d("Result","okk")

                //val nomer = dataPointList[Integer.valueOf(position!!)].name
                //val mId = dataPointList[Integer.valueOf(position!!)].id
                val customDialog = CustomDialog(activity!!, "nomer", "nomer.toString()")
             //   customDialog!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
               // customDialog?.show()
            }
        })
        binding.recyclerView.adapter = menuListAdapter
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)*/


        offerListAdapter =
            OfferListAdapter(object :OfferListAdapter.ClickHandler{
                override fun onItemClicked(item: GetTestDataResponse.TestData.Cupons) {
                }

            })
        binding.recyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.layoutManager = layoutManager
        val decorator = DividerItemDecoration(
            ContextThemeWrapper(this.context, R.style.AppTheme),
            LinearLayoutManager.VERTICAL
        )
        //  binding.recyclerView.addItemDecoration(decorator)
        binding.recyclerView.adapter = offerListAdapter

        offerListAdapter.submitList(data)

        return binding.root
    }

    override fun onNumberClick(numbers: Int?) {


    }


}

class CustomDialog(activity: FragmentActivity, nomer: Any, toString: Any) {

}
