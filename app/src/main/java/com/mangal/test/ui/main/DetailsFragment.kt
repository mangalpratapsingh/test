package com.mangal.test.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.mangal.test.R
import com.mangal.test.data.GetTestDataResponse
import com.mangal.test.databinding.FragmentDetailsBinding
import com.mangal.test.di.module.GlideApp
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment

class DetailsFragment(private val data: GetTestDataResponse.TestData) : DaggerFragment() {


    private lateinit var binding: FragmentDetailsBinding


    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false)
        binding.setLifecycleOwner(this)
        binding.txtDesc.text = data.descriptionBody


        GlideApp.with(context!!)
            .load(data.decriptionImage)
            .placeholder(R.drawable.ic_launcher_background)
            .fitCenter()
            .into(binding.imgeOne)



        return binding.root
    }

}
