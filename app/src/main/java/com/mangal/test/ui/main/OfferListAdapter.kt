package com.mangal.test.ui.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mangal.test.R
import com.mangal.test.data.GetTestDataResponse
import com.mangal.test.databinding.OfferItemBinding


class OfferListAdapter(private val clickHandler: ClickHandler) :
    ListAdapter<GetTestDataResponse.TestData.Cupons, OfferListAdapter.MyViewHolder>(Callback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: OfferItemBinding =
            DataBindingUtil.inflate(inflater, R.layout.offer_item, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(clickHandler, getItem(position))
    }

    class Callback : DiffUtil.ItemCallback<GetTestDataResponse.TestData.Cupons>() {
        override fun areItemsTheSame(
            p0: GetTestDataResponse.TestData.Cupons,
            p1: GetTestDataResponse.TestData.Cupons
        ): Boolean {
            return p0.title == p1.title
        }

        override fun areContentsTheSame(
            p0: GetTestDataResponse.TestData.Cupons,
            p1: GetTestDataResponse.TestData.Cupons
        ): Boolean {
            return p0 == p1
        }
    }

    class MyViewHolder(private val binding: OfferItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(clickHandler: ClickHandler, item: GetTestDataResponse.TestData.Cupons) {
            binding.couponItem = item
           // binding.estimateSaving.text = "Estimate Saving:" + item.price
            binding.root.setOnClickListener {
                clickHandler.onItemClicked(item)
            }
        }
    }

    interface ClickHandler {
        fun onItemClicked(item: GetTestDataResponse.TestData.Cupons)
    }

}