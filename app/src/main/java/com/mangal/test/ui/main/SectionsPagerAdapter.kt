package com.mangal.test.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import com.mangal.test.R
import com.mangal.test.di.module.GlideApp


class SectionsPagerAdapter(private val context: Context, private val data: List<String>) :
    PagerAdapter() {

    private var inflater: LayoutInflater? = null


    override fun getCount(): Int {
        return data.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val itemView: View =
            LayoutInflater.from(context).inflate(R.layout.image_slider_item, container, false)

        val imageView: ImageView =
            itemView.findViewById<View>(R.id.slider_image_view) as ImageView

        // Glide.with(context).load(data[position]).into(imageView)
        //imageView.setImageResource(images.get(position))


        GlideApp.with(context!!)
            .load(data[position])
            // .placeholder(R.drawable.ic_launcher_background)
            .fitCenter()
            .into(imageView)


        container.addView(itemView)

        //listening to image click

        //listening to image click
        imageView.setOnClickListener(View.OnClickListener {
            Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG)
                .show()
        })

        return itemView
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)

    }
}