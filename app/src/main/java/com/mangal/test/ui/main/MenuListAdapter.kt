package com.mangal.test.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mangal.test.R
import com.mangal.test.data.User

class MenuListAdapter(context: Context, number: ArrayList<User>?, onNumberListener: OnNumberListener) : RecyclerView.Adapter<MenuListAdapter.ViewHolder>() {
    private var dataPointList: ArrayList<User>? = ArrayList<User>()
    private val mOnNumberListener: OnNumberListener?
    private val mContext: Context


    inner class ViewHolder(itemView: View, onNumberListener: OnNumberListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val textPhone: TextView = itemView.findViewById<View>(R.id.TvPhone) as TextView
        private var onNumberListener: OnNumberListener = onNumberListener

        override fun onClick(view: View?) {
            onNumberListener?.onNumberClick(adapterPosition)
        }


        init {
            itemView.setOnClickListener(this)
        }
    }

    private val mInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(mContext)
        val view: View = inflater.inflate(R.layout.list_data, parent, false)
        return ViewHolder(view, mOnNumberListener!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textPhone.text = dataPointList!![position].name
        /*
        holder.textPhone.setOnClickListener {
            Toast.makeText(mContext, "clicked", Toast.LENGTH_SHORT).show()

        }
         */


    }

    override fun getItemCount(): Int {
        return dataPointList?.size ?: -1
    }

    interface OnNumberListener {
        fun onNumberClick(numbers: Int?)
    }

    companion object {
        private val TAG = MenuListAdapter::class.java.simpleName
    }

    init {
        dataPointList = number
        mOnNumberListener = onNumberListener
        mContext = context
    }

}