package com.mangal.test.di.module

import com.mangal.test.MainActivity
import com.mangal.test.login.LoginActivity
import com.mangal.test.login.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilderModule {


	@ContributesAndroidInjector
	internal abstract fun contributeMainActivity(): MainActivity

	@ContributesAndroidInjector
	internal abstract fun contributeSplashActivity(): SplashActivity

	@ContributesAndroidInjector
	internal abstract fun contributeLoginActivity(): LoginActivity

}