package com.mangal.test.di.component

import android.app.Application
import com.mangal.test.TestApplication
import com.mangal.test.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilderModule::class,
        ApiServiceModule::class,
        FragmentBuilderModule::class,
        NetworkModule::class,
        ViewModelBuilderModule::class]
)
interface AppComponent : AndroidInjector<TestApplication> {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun create(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}