package com.mangal.test.di.module

import com.mangal.test.data.ApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module(includes = [NetworkModule::class])
class ApiServiceModule {

	@Provides
	@Singleton
	fun provideRetrofitApiService(retrofit: Retrofit): ApiService {
		return retrofit.create(ApiService::class.java)
	}


}