package com.mangal.test.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mangal.test.di.TestViewModelFactory
import com.mangal.test.di.ViewModelKey
import com.mangal.test.ui.main.TestViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelBuilderModule {


    @Binds
    @IntoMap
    @ViewModelKey(TestViewModel::class)
    abstract fun bindRegisterOneViewModel(viewModel: TestViewModel): ViewModel


    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: TestViewModelFactory): ViewModelProvider.Factory



}