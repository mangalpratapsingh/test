package com.mangal.test.di.module


import com.mangal.test.ui.main.DetailsFragment
import com.mangal.test.ui.main.OffersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

	/**
	 * Common flow fragment list
	 */


	@ContributesAndroidInjector
	abstract fun contributeOffersFragment(): OffersFragment

	@ContributesAndroidInjector
	abstract fun contributeDetailsFragment(): DetailsFragment




}