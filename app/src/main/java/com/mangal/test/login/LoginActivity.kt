package com.mangal.test.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.mangal.test.MainActivity
import com.mangal.test.R
import com.mangal.test.data.ApiService
import com.mangal.test.data.LoginRequest
import com.mangal.test.data.LoginResponse
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity() {


    @Inject
    lateinit var apiService: ApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        buttonLogin.setOnClickListener {
            if (validate()) {
                checkLogin()
            }
        }
    }

    private fun checkLogin() {
        progressBar.visibility = View.VISIBLE
        val loginData = LoginRequest(email = editTextEmail.text.toString(),password = editTextPassword.text.toString())
        apiService.postLoginData(email = editTextEmail.text.toString(),password = editTextPassword.text.toString()
        ).enqueue(object : retrofit2.Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("Login",t.localizedMessage)
                progressBar.visibility = View.GONE

            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                progressBar.visibility = View.GONE
                Log.d("Login",response.toString())
                if(response.body()!!.source().toString().contains("Invalid Email")){
                    Toast.makeText(this@LoginActivity,"Invalid Email password combination for login.",Toast.LENGTH_SHORT).show()

                }else{
                    val intent = Intent(this@LoginActivity,MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }

        })
    }

    private fun validate(): Boolean {
        if (!editTextEmail.text.toString().isNotEmpty()) {
            editTextEmail.error = "this field is mandatory"
            return false
        }
        if (!editTextPassword.text.toString().isNotEmpty()) {
            editTextPassword.error = "this field is mandatory"

            return false
        }
        return true

    }


}