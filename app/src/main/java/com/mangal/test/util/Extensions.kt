package com.mangal.test.util

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.NonNull
import com.google.android.material.snackbar.Snackbar

fun View.showSnack(
    message: String,
    length: Int = Snackbar.LENGTH_SHORT
) {
    try {
        if (message.length > 1) {
            Snackbar.make(this, message, length)
                .apply {
                    setAction("Dismiss") { dismiss() }
                    setActionTextColor(Color.CYAN)
                    show()
                }
        }
    } catch (e: Exception) {
        // Ignored
    }
}


fun androidx.appcompat.app.AppCompatActivity.showToast(message: String) {
    runOnUiThread {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}


fun View.showToast(
    message: String,
    length: Int = Toast.LENGTH_SHORT
) {
    if (message.length > 1) {
        Toast.makeText(this.context, message, length)
            .show()
    }
}

fun androidx.fragment.app.Fragment.dismissKeyboard() {
    try {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(view?.windowToken, 0)
    } catch (e: Exception) {
        // Ignored
    }
}

fun ProgressBar.showProgressBar(@NonNull status: Boolean, @NonNull window: Window) {
    try {
        if (status) {
            this.visibility = View.VISIBLE
            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            )
        } else {
            this.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    } catch (e: Exception) {
        // Ignored
    }
}

